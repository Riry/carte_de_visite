<?php

//On récupère les informations du formulaire

$nom = $_POST['name'];
$metier = $_POST['job'];
$telephone = $_POST['phone'];
$mail = $_POST['mail'];
$adresse = $_POST['adress'];
$site = $_POST['website'];


//On vérifie si certaine doivent être afficher ou non

function afficherInformation($info, $class){
    if(isset($info)){
        echo "<p class='$class'> $info </p>";
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <title>Carte de Visite</title>
</head>
<body>
    <div class="visit-card">
        <div class='vc-identity'>
            <p class='vc-name'><?php echo $nom?></p>
            <?php 
            //On vérifie si le métier à été rentré, et si c'est le cas on l'affiche en dessous du nom
            afficherInformation($metier, 'vc-job');?>
        </div>
        <div class='vc-information'>
            <?php afficherInformation($adresse, 'vc-adress');
            afficherInformation($telephone, 'vc-phone');?>
            <p class='vc-mail'><?php echo $mail?></p>
            <?php afficherInformation($site, 'vc-site')?>
        </div>
    </div>
</body>
</html>

