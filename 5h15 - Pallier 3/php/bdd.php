<?php

$dsn = "mysql:dbname=carte-de-visite;host=localhost;charset=UTF8;";
$username = "root";
$password = "";



function executerSQL($sql){
    //On récupère les variables précédentes
    global $dsn;
    global $username;
    global $password; 

    $pdo = new PDO($dsn, $username, $password);
    $result = $pdo->query($sql);
    $rows = $result->fetch();
    return $rows; 
}

function fetchEverything($table){
    //On récupère les variables précédentes
    global $dsn;
    global $username;
    global $password; 
    
    //On écrit la commande SQL
    $sql = "SELECT * FROM $table";

    //On ouvre notre base de données
    $pdo = new PDO($dsn, $username, $password);
    $result = $pdo->query($sql);
    //On récupère tout
    $rows = $result->fetchAll(PDO::FETCH_ASSOC);
     return $rows; 
}   


function insert($table, $colonneNames, $values){
    try{

      //On récupère les variables précédentes

        global $dsn;
        global $username;
        global $password;
      //On ouvre notre base de donnée
        $connexion = new PDO($dsn, $username, $password);
        $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // On écrit notre commande sql

        $sql = "INSERT INTO $table ($colonneNames) VALUES ($values)"; 

        // On execute le SQL
        $connexion->exec($sql);
        echo 'Nouvelle ligne créée';
        // On gère les erreurs
    } catch(PDOException $error){
        echo $sql."<br>".$error->getMessage();
    }
    $conn = null;
    
}

?>
