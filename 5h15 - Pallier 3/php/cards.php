<?php

include_once 'bdd.php';
include_once 'function.php';

$id = $_POST['id'];

$infos = executerSQL("SELECT * FROM informations WHERE `id`=$id");

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <title>Carte de visiteur</title>
</head>
<body>

    <!-- Carte de visite -->
    <div class="visit-card">

        <!-- Nom, prénom, métiers -->
        <div class='vc-identity'>
            <p class='vc-name'><?php echo $infos['nom']?></p>
            <?php 
            //On vérifie si le métier à été rentré, et si c'est le cas on l'affiche en dessous du nom
            afficherInformation($infos['metier'], 'vc-job');?>
        </div>
        <!-- Fin Nom, prénom, métiers -->

        <!-- Image et contenue -->
        <div class='vc-content'>

        <!-- On affiche l'image-->
            <?php echo "<img src='".$infos['imageUrl']."' alt='Votre Photo' class='vc-image-carte'>";?>

        <!-- On affiche le contenue de la carte -->
            <div class='vc-information'>
                <?php afficherInformation($infos['adresse'], 'vc-adress');
                afficherInformation($infos['telephone'], 'vc-phone');?>
                <p class='vc-mail'><?php echo $infos['mail']?></p>
                <?php afficherInformation($infos['site'], 'vc-site')?>
            </div>
        </div>
        <!-- Fin Image et contenue -->

    </div>
    <!-- Fin Carte de visite -->
</body>
</html>