<?php

include_once 'function.php';


//On récupère les informations du formulaire

$infos = ['nom' => $_POST['name'],
'metier' => $_POST['job'],
'telephone' => $_POST['phone'],
'mail' => $_POST['mail'],
'adresse' => $_POST['adress'],
'site' => $_POST['website']];

//On récupère la photo

$image = $_FILES['imgUpload'];

// On vérifie si l'image existe

if(isset($image)){
     //On indique le chemin où va devoir s'enregistrer l'image 
    $cheminImage = '../photo/'.basename($image['name']);

    // On vérifie si la taille de l'image n'est pas trop grande (1 Mo)
    $telechargerImage = true;
    
    if($image['size'] > 1000000){
        $telechargerImage = false;
    }
}


// On upload l'image
if($telechargerImage){
    move_uploaded_file($image['tmp_name'], $cheminImage);
}
else{
    echo 'Votre image ne respecte pas les conditions et n\'a par conséquent pas été implémentée';
}


?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <title>Carte de Visite</title>
</head>
<body>

    <!-- Carte de visite -->
    <div class="visit-card">

        <!-- Nom, prénom, métiers -->
        <div class='vc-identity'>
            <p class='vc-name'><?php echo $infos['nom']?></p>
            <?php 
            //On vérifie si le métier à été rentré, et si c'est le cas on l'affiche en dessous du nom
            afficherInformation($infos['metier'], 'vc-job');?>
        </div>
        <!-- Fin Nom, prénom, métiers -->

        <!-- Image et contenue -->
        <div class='vc-content'>

        <!-- On affiche l'image-->
            <?php echo "<img src='$cheminImage' alt='Votre Photo' class='vc-image-carte'>";?>

        <!-- On affiche le contenue de la carte -->
            <div class='vc-information'>
                <?php afficherInformation($infos['adresse'], 'vc-adress');
                afficherInformation($infos['telephone'], 'vc-phone');?>
                <p class='vc-mail'><?php echo $infos['mail']?></p>
                <?php afficherInformation($infos['site'], 'vc-site')?>
            </div>
        </div>
        <!-- Fin Image et contenue -->

    </div>
    <!-- Fin Carte de visite -->

    <!-- Formulaire pour envoyer les informations à mettre dans la base de données afin d'afficher toutes les cartes créées-->
    <form action = 'publish.php' method = 'POST'>

    <!-- Boucle pour envoyer les infos -->
        <?php
        foreach($infos as $key => $value){
            echo "<input type='hidden' name='$key' value='$value'>  </input>";
        }

    //On envoie l'image si elle existe
        if(isset($image)){
            echo "<input type='hidden' name='imageURL' value='$cheminImage'></input>";
        }

        ?>
        <button type='submit' class='btn-card-generator'> Publier ma carte </button>
    </form>
</body>
</html>

