<?php


//On récupère les informations du formulaire

$nom = $_POST['name'];
$metier = $_POST['job'];
$telephone = $_POST['phone'];
$mail = $_POST['mail'];
$adresse = $_POST['adress'];
$site = $_POST['website'];


//On récupère la photo

$image = $_FILES['imgUpload'];

// On indique le chemin où va devoir s'enregistrer l'image

$cheminImage = '../photo/'.basename($image['name']);

// On vérifie si la taille de l'image n'est pas trop grande (1 Mo)

$telechargerImage = true;

if($image['size'] > 1000000){
    $telechargerImage = false;
}

// On upload l'image
if($telechargerImage){
    move_uploaded_file($image['tmp_name'], $cheminImage);
}
else{
    echo 'Votre image ne respecte pas les conditions et n\'a par conséquent pas été implémentée';
}
//FUNCTION 

//Fonction pour vérifier l'existence d'une information et l'afficher

function afficherInformation($info, $class){
    if(isset($info)){
        echo "<p class='$class'> $info </p>";
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <title>Carte de Visite</title>
</head>
<body>
    <div class="visit-card">
        <div class='vc-identity'>
            <p class='vc-name'><?php echo $nom?></p>
            <?php 
            //On vérifie si le métier à été rentré, et si c'est le cas on l'affiche en dessous du nom
            afficherInformation($metier, 'vc-job');?>
        </div>
        <div class='vc-content'>

            <?php echo "<img src='$cheminImage' alt='Votre Photo' class='vc-image-carte'>";?>

            <div class='vc-information'>
                <?php afficherInformation($adresse, 'vc-adress');
                afficherInformation($telephone, 'vc-phone');?>
                <p class='vc-mail'><?php echo $mail?></p>
                <?php afficherInformation($site, 'vc-site')?>
            <div>
        </div>
    </div>
</body>
</html>

